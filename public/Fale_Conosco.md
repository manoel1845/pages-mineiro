# Fale Conosco

Criamos alguns canais de contato direto com o time responsável pela plataforma. Veja as opções:
  
## Chamados

O atendimento de chamados é feito no [ServiceNow](https://itau.service-now.com/tech).

Chamados devem ser abertos através do menu **Catálogo** -> **Serviços de TI Avançados** -> **Cloud Privada** -> **OpenShift (PaaS)**.

Também é possível localizar um item de catálogo para abertura de chamado digitando **paas** no campo de pesquisa.

### Troubleshooting

!> **Importante!** Para problemas no `deploy` de aplicações, consulte antes os [logs e eventos](/Features/Observability?id=logs-e-eventos) da aplicação no Splunk antes de abrir o chamado. Procure também na nossa [FAQ](/FAQ) para ver se tem alguma resposta para sua dúvida, na Base de Conhecimento ou pesquise por alguma mensagem de erro que encontrou. 

!> **Importante!** Se mesmo assim, precisar ajuda, procure enviar os logs e eventos (de preferência do Splunk) pertinentes com as dúvidas para um melhor atendimento e mais agilidade

Para solicitar auxílio em *troubleshooting* na Plataforma, [clique aqui](https://itau.service-now.com/tech?id=sc_cat_item&sys_id=d5cf17e11beb10d0f7f10f60f54bcb84&sysparm_category=5c11eeb21b6e50100aff86afe54bcbf2).

### Dúvidas

Para dúvidas, reportar Bugs na Plataforma ou alterações da Documentação, [clique aqui](https://itau.service-now.com/tech?id=sc_cat_item&sys_id=45cb78291b1bd4101941dca0f54bcb5a&sysparm_category=5c11eeb21b6e50100aff86afe54bcbf2).

## Atendimento / Plantão

Para saber mais sobre os horários e contatos tanto dos integrantes quanto do plantão, [clique aqui](https://confluencecorp.ctsp.prod.cloud.ihf/display/ComunCloud/2020+PAAS+-+Atendimento+Plataforma)

## Autosserviço

Consulte as opções de autosserviço que estamos disponibilizando no menu lateral esquerdo ou clicando [aqui](/Autosservico/Autosservico)

> [!NOTE]
> Estas opções estão sendo disponibilizadas diretamente no ServiceNow!

## Novas Features

Para sugerir novas features, preencha o formulário abaixo:

<iframe width="640px" height= "2000px" src= "https://forms.office.com/Pages/ResponsePage.aspx?id=oGkWWT8YpUmY9Jqg0LY9gbpxMzl5fcZKhXACtC6YLxtUMEVGWEc4V1VXMUVMOTdPSDhURTVRODBNTCQlQCN0PWcu&embed=true" frameborder= "0" marginwidth= "0" marginheight= "0" style= "border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>



<!-- LIKERT -->
